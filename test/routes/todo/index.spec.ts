import { app } from "../../server";
import request from "supertest";
import { expect } from "chai";

describe("Todo API", function () {
  it("should create a new todo", async () => {
    const todoData = {
      title: "Test Todo",
      description: "This is a test todo item",
    };
    const response = await request(app)
      .post("/todo")
      .send(todoData)
      .expect(200);

    expect(response.body).to.have.property("id");
    expect(response.body.title).to.equal(todoData.title);
    expect(response.body.description).to.equal(todoData.description);
  });

  it("should get list of todos", async () => {
    const response = await request(app).get("/todo").expect(200);

    expect(response.body).to.be.an("array");
  });

  it("should get a specific todo", async () => {
    // Create a new todo
    const todoData = {
      title: "Test Todo",
      description: "This is a test todo item",
    };
    const createResponse = await request(app)
      .post("/todo")
      .send(todoData)
      .expect(200);

    const todoId = createResponse.body.id;

    // Get the created todo
    const response = await request(app).get(`/todo/${todoId}`).expect(200);

    expect(response.body.id).to.equal(todoId);
    expect(response.body.title).to.equal(todoData.title);
    expect(response.body.description).to.equal(todoData.description);
  });

  it("should update a todo", async () => {
    // Create a new todo
    const todoData = {
      title: "Test Todo",
      description: "This is a test todo item",
    };
    const createResponse = await request(app)
      .post("/todo")
      .send(todoData)
      .expect(200);

    const todoId = createResponse.body.id;

    // Update the todo
    const updatedData = {
      title: "Updated Todo",
      description: "This is an updated test todo item",
      completed: true,
    };
    const updateResponse = await request(app)
      .patch(`/todo/${todoId}`)
      .send(updatedData)
      .expect(200);

    expect(updateResponse.body.id).to.equal(todoId);
    expect(updateResponse.body.title).to.equal(updatedData.title);
    expect(updateResponse.body.description).to.equal(updatedData.description);
    expect(updateResponse.body.completed).to.equal(updatedData.completed);
  });

  it("should delete a todo", async () => {
    // Create a new todo
    const todoData = {
      title: "Test Todo",
      description: "This is a test todo item",
    };
    const createResponse = await request(app)
      .post("/todo")
      .send(todoData)
      .expect(200);

    const todoId = createResponse.body.id;

    // Delete the todo
    await request(app).delete(`/todo/${todoId}`).expect(200);

    // Verify that the todo is deleted
    await request(app).get(`/todo/${todoId}`).expect(404);
  });
});
