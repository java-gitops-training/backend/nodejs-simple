import { Application } from "express";
import createServer from "../src/server";

import * as dotenv from "dotenv";

dotenv.config({ path: "./.env.test" });

let app: Application;

before(async () => {
  app = await createServer();
});

export { app };
