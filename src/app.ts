import "dotenv/config";
import createServer from "./server";

const port: number = Number(process.env.API_SERVER_PORT || 4000);

const startServer = async () => {
  const app = await createServer();

  app.listen(port, async () => {
    console.log(`App is running at http://::${port}`);
  });
};

process.on("uncaughtException", (err: any) => {
  console.log("UNCAUGHT EXCEPTION! 🔥");
  console.log(err.name, err.message);
  console.log(err);
});

startServer();

process.on("unhandledRejection", (err: any) => {
  console.log("UNHANDLER REJECTION! 🔥");
  console.log(err.name, err.message);
});
