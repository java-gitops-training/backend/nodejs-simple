import "dotenv/config";
import express, { Application, Request, Response, NextFunction } from "express";
import routes from "./routes/index";
import errorHandler from "./middleware/errorHandler";
import cors from "cors";
import { ErrorStatus } from "./middleware/error.model";

export default async function createServer() {
  const app: Application = express();

  app.use(cors());
  app.use(express.json());
  app.use(
    express.urlencoded({
      extended: false,
      limit: "50mb",
    })
  );

  app.get(
    "/generate_204",
    (_req: Request, res: Response, _next: NextFunction) => {
      res.status(ErrorStatus.NO_CONTENT).send();
    }
  );

  app.use(routes);

  app.use(errorHandler);

  return app;
}
