import { Request, Response, NextFunction, ErrorRequestHandler } from "express";
import { ErrorModel, ErrorStatus } from "./error.model";

const handleCastErrorDB = (err: any) => {
  const message = `Invalid ${err.path}: ${err.value}`;
  return new ErrorModel(message, ErrorStatus.BAD_REQUEST);
};

const handleDuplicateFieldsDB = (err: any) => {
  // const value = err.errmsg.match(/(["'])(\\?.)*?\1/);
  const message = `Duplicate field value: ${err.errmsg}. Please use another value!`;
  return new ErrorModel(message, ErrorStatus.BAD_REQUEST);
};

const handleInvalidTimezoneDB = (err: any) => {
  const value = err.errmsg.match(/(["'])(\\?.)*?\1/)[0];
  const message = `Invalid timezone value: ${value}. Please use another value!`;
  return new ErrorModel(message, ErrorStatus.BAD_REQUEST);
};

const handleInvalidParameter = (err: any) => {
  return new ErrorModel("Invalid Type", ErrorStatus.BAD_REQUEST, err);
};

const handleEntityTooLarge = (_err: any) => {
  return new ErrorModel("Entity Too Large", ErrorStatus.BAD_REQUEST);
};

const handleError = <ErrorRequestHandler>(
  function (
    err: TypeError | ErrorModel | any,
    _req: Request,
    res: Response,
    _next: NextFunction
  ) {
    let error: any = err;

    if (!(err instanceof ErrorModel)) {
      if (process.env.NODE_ENV === "development") {
        console.log(error);
      }

      // Express entity too large
      if (error.type === "entity.too.large") {
        return res
          .status(ErrorStatus.BAD_REQUEST)
          .send(handleEntityTooLarge(error));
      }

      // Detect mongoose errors
      if (error.name === "CastError")
        return res
          .status(ErrorStatus.BAD_REQUEST)
          .send(handleCastErrorDB(error));
      if (error.code === 11000)
        return res
          .status(ErrorStatus.BAD_REQUEST)
          .send(handleDuplicateFieldsDB(error));
      if (error.code === 40485)
        return res
          .status(ErrorStatus.BAD_REQUEST)
          .send(handleInvalidTimezoneDB(error));

      // Detect input validation errors
      if (Array.isArray(error)) {
        const errResult = error.filter((item) =>
          [
            "invalid_type",
            "invalid_string",
            "invalid_enum_value",
            "too_small",
            "too_big",
            "custom",
          ].includes(item.code)
        );

        return res
          .status(ErrorStatus.BAD_REQUEST)
          .send(handleInvalidParameter(errResult));
      }

      return res
        .status(ErrorStatus.INTERNAL_SERVER_ERROR)
        .send(
          new ErrorModel(
            error.message.toString(),
            ErrorStatus.INTERNAL_SERVER_ERROR,
            process.env.NODE_ENV === "development"
              ? { stack: error.stack.toString() }
              : {}
          )
        );
      // return res.status(ErrorStatus.INTERNAL_SERVER_ERROR).send();
    } else {
      if (process.env.NODE_ENV === "development") {
        console.log(error);
      }

      console.log(error);
      return res.status((error as ErrorModel).status).send(error);
    }
  }
);

export default handleError;
