import { NextFunction, Request, Response } from "express";
import { catchAsync } from "../../utils/catchAsync";
import { Todo } from "./todo.types";
import { CreateTodoInput, GetTodoInput, UpdateTodoInput } from "./todo.schema";

let todos: Todo[] = [];

export const getListTodoHandler = catchAsync(
  async (_req: Request, res: Response, _next: NextFunction) => {
    return res.send(todos);
  }
);

export const createTodoHandler = catchAsync(
  async (
    req: Request<CreateTodoInput["body"]>,
    res: Response,
    _next: NextFunction
  ) => {
    const { title, description } = req.body;

    const newTodo: Todo = {
      id: todos.length + 1,
      title,
      description,
      completed: false,
    };
    todos.push(newTodo);

    return res.send(newTodo);
  }
);

export const updateTodoHandler = catchAsync(
  async (
    req: Request<UpdateTodoInput["params"]>,
    res: Response,
    _next: NextFunction
  ) => {
    const { id } = req.params;
    const { title, description, completed } = req.body;

    const todoIndex = todos.findIndex((todo) => todo.id === parseInt(id));
    if (todoIndex === -1) {
      return res.status(404).send("Todo not found");
    }
    todos[todoIndex] = Object.assign(
      {},
      todos[todoIndex],
      title && { title },
      description && { description },
      completed && { completed }
    );

    return res.send(todos[todoIndex]);
  }
);

export const getTodoHandler = catchAsync(
  async (
    req: Request<GetTodoInput["params"]>,
    res: Response,
    _next: NextFunction
  ) => {
    const { id } = req.params;

    const todo = todos.find((todo) => todo.id === parseInt(id));
    if (!todo) {
      return res.status(404).send("Todo not found");
    }

    return res.send(todo);
  }
);

export const deleteTodoHandler = catchAsync(
  async (
    req: Request<UpdateTodoInput["params"]>,
    res: Response,
    _next: NextFunction
  ) => {
    const { id } = req.params;

    todos = todos.filter((todo) => todo.id !== parseInt(id));

    return res.send();
  }
);
