import { object, string, TypeOf, boolean } from "zod";

const payload = {
  body: object({
    title: string({
      required_error: "Name is required",
    }),
    description: string().optional(),
    completed: boolean().optional(),
  }),
};

const payloadUpdate = {
  body: object({
    title: string().optional(),
    description: string().optional(),
    completed: boolean().optional(),
  }),
};

const params = {
  params: object({
    id: string({
      required_error: "Todo Id is required",
    }),
  }),
};

export const createTodoSchema = object({
  ...payload,
});

export const updateTodoSchema = object({
  ...payloadUpdate,
  ...params,
});

export const deleteTodoSchema = object({
  ...params,
});

export const getTodoSchema = object({
  ...params,
});

export type CreateTodoInput = TypeOf<typeof createTodoSchema>;
export type UpdateTodoInput = TypeOf<typeof updateTodoSchema>;
export type GetTodoInput = TypeOf<typeof getTodoSchema>;
export type DeleteTodoInput = TypeOf<typeof deleteTodoSchema>;
