### STAGE 1: Install Dependencies and Build ###
FROM node:alpine as builder

WORKDIR /build-stage

COPY package*.json ./

RUN npm ci

COPY tsconfig.json ./
COPY src ./src

RUN npm run build

### STAGE 2: Run ###
FROM node:alpine as runner

WORKDIR /usr/src/app

COPY --from=builder /build-stage/node_modules ./node_modules
COPY --from=builder /build-stage/dist ./

ENV NODE_ENV=production
ENV API_SERVER_PORT=4000

CMD [ "node", "app.js" ]
